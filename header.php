<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
        <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.png"/>
        <?php html5blank_styles(); ?>
        <?php html5blank_header_scripts(); ?>
        <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>