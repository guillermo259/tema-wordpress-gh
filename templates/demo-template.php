<?php 

/* Template Name: DEMO DE PLANTILLA */

?>
<?php
        global $wpdb;
        //$table_name = "enlaces_comerciales";
        //$rows = $wpdb->get_results("SELECT enlace_cedula,enlace_nombre from $table_name");

        $pagenum = filter_input(INPUT_GET, 'pagenum') ? absint(filter_input(INPUT_GET, 'pagenum')) : 1;
        $table_name = "enlaces_comerciales";
        $limit = 20; // number of rows in page
        $offset = ( $pagenum - 1 ) * $limit;
        $total = $wpdb->get_var( "SELECT COUNT(`enlace_cedula`) FROM $table_name" );
        $num_of_pages = ceil( $total / $limit );
        $rows = $wpdb->get_results( "SELECT * FROM $table_name LIMIT $offset, $limit" );
        $page_links = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __( '&laquo;', 'text-domain' ),
            'next_text' => __( '&raquo;', 'text-domain' ),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );
        ?>
<table class='table table-hover table-bordered'>
    <tr>
        <th class="text-center">Cedula del enlace</th>
        <th class="text-center">Nombre</th>
        <th class="text-center">Actualizar usuario</th>
    </tr>
    <?php foreach ($rows as $row) { ?>
        <tr>
            <td class="manage-column ss-list-width"><?php echo $row->enlace_cedula; ?></td>
            <td class="manage-column ss-list-width"><?php echo $row->enlace_nombre; ?></td>
            <td class="text-center"><a class="btn btn-primary" href="<?php echo admin_url('admin.php?page=enlaces_update&id=' . $row->enlace_cedula); ?>">Actualizar</a></td>
        </tr>
    <?php } ?>
</table>
<div>
    <div class="row">
        <div class="col-md-12 text-center">
            <?php
            if ( $page_links ) {
                echo '<nav aria-label="Page navigation"><ul class="pagination"><li>' . $page_links . '</li></ul></nav>';
            }
            ?>
        </div>
    </div>
</div>
</div>