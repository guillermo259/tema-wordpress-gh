<?php

/* Template Name: TEST TEMPLATE */

get_header();
wp_head();

?>

    <form class="formulario_indicadores" method="POST" >
        <div class="busq_category">
            <label  class="informes_label">Seleccione la Fecha a Consultar</label>
            <!--<label for="Enfoque_list" >Mes</label>-->
            <select hidden name="mes" id="mes">
                <option value="01">Enero</option>
                <option value="02">Febrero</option>
                <option value="03">Marzo</option>
                <option value="04">Abril</option>
                <option value="05">Mayo</option>
                <option value="06">Junio</option>
                <option value="07">Julio</option>
                <option value="08">Agosto</option>
                <option value="09">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
            </select>
            <label for="Enfoque_list" class="informes_label3" >Año</label>
            <select name="anno" id="anno">
                <option value="Selecciona un año">Selecciona un año</option>
                <option value="2004">2004</option>
                <option value="2005">2005</option>
                <option value="2006">2006</option>
                <option value="2007">2007</option>
                <option value="2008">2008</option>
                <option value="2009">2009</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
                <option value="2013">2013</option>
                <option value="2014">2014</option>
                <option value="2015">2015</option>
                <option value="2016">2016</option>
                <option value="2017">2017</option>
                <option value="2018">2018</option>
            </select>
            <input type="submit" name="Consultar_boletin" value="Consultar">
        </div>


    </form>

<?php

//Funcion que devuelve el mes dependiendo del numero del mes enviado a
function get_month($value) {
    if ($value==01) {
        return "Enero";
    } else if ($value==02) {
        return "Febrero";
    } else if ($value==03) {
        return "Marzo";
    } else if ($value==04) {
        return "Abril";
    } else if ($value==05) {
        return "Mayo";
    } else if ($value==06) {
        return "Junio";
    } else if ($value==07) {
        return "Julio";
    } else if ($value==8) {
        return "Agosto";
    } else if ($value==9) {
        return "Septiembre";
    } else if ($value==10) {
        return "Octubre";
    } else if ($value==11) {
        return "Noviembre";
    } else if ($value==12) {
        return "Diciembre";
    }
}

?>

<?php

if (isset($_POST['mes']) ) {

    $mes=$_POST['01'];
    $anno=$_POST['anno'];

     //echo "MES: " .$mes;
     //echo "<br>";
     //echo "AÑO " .$anno;
    //echo "<br>";
}else{

    $fecha_actual_mes= date('m');
    $fecha_actual_anno= date('Y');

    $mes=$fecha_actual_mes;
    $anno=$fecha_actual_anno;
    //echo $mes;
    //echo $anno;
}

$boletin_descargar = array();

$directorio_archivos="file:///C:/wamp/www/plugin-test/portal/wp-content/uploads/documentos/boletines/";
$dir = opendir($directorio_archivos);
$files = array();

while ($current = readdir($dir)){
    if( $current != "." && $current != "..") {
        if(is_dir($directorio_archivos.$current)) {
            showFiles($directorio_archivos.$current.'/');
        }
        else {
            $files[] = $current;
        }
    }
}
sort($files);

$anno_1 = array();
$a1= 0;

for($i=0; $i<count( $files ); $i++) {

    //Nombre Archivo
    $nombre_archivo = $files[$i];
    //Saco la fecha  y el nombre en variables distintas
    $resultado=explode('-', $nombre_archivo);
    //Nombre
    $nombre_boletin = $resultado[0];

    $archivo=explode('.', $resultado[1]);
    //Fecha sin el .pdf
    $fecha_boletin=$archivo[0];
    //Obtengo el año
    $fecha_año= substr($fecha_boletin, 0, 4);
    //Obtengo el mes
    $fecha_mes= substr($fecha_boletin, 4, 6);

    //echo "<p>" . $nombre_archivo. "</p>";
    if ($fecha_año == $anno) {
        $anno_1[$a1]['nombre'] = $nombre_archivo;
        $anno_1[$a1]['mes']=$fecha_mes;
        $anno_1[$a1]['año']=$fecha_año;
        $a1++;
    }

    if ($anno==$fecha_año and $mes==$fecha_mes) {

    }else{
        $fecha_mes=$mes;
        $fecha_año=$anno;
    }
    //echo $fecha_boletin . "<br>";
}


//Muestro el boletin que coincide con las varibles enviadas  por post
if ($nombre_archivo!="." and $nombre_archivo!=".." and $fecha_año==$anno and $fecha_mes==$mes ) {

    // echo $nombre_archivo;
    //echo 'Nombre:  '.$nombre_boletin.'<br>  fecha '.$fecha_boletin.'<br>  el año es '.$fecha_año.'<br> El mes es '.$fecha_mes;

    $nombre_boletin_final="Boletin-".$fecha_año."-".$fecha_mes;

    $nombre_archivo_descarga="boletin-".$fecha_año.$fecha_mes.".pdf";

    $boletin_descarga=$nombre_archivo_descarga;

    $boletin_descargar[]=$nombre_boletin_final;

    //muestro si el boletin existe
    $site_url = get_home_url()."/wp-content/uploads/documentos/boletines/";

    $url_descarga = $site_url.$nombre_archivo_descarga;
    //echo $url_descarga;

    //echo "<pre>";
    //var_dump($anno_1);
    //echo "</pre>";

    echo "<div style='font-size: 15px;'><p>Mostrando resultados de <span id='mes'> </span> <span id='año'></span></p></div> ";

    echo "<ul>";
    if (empty($anno_1)) {
        echo "<li class='titulo_boletin'>No hay solicitudes de Inscripcion para esta fecha<li>";
    }

    for($i=0; $i<count( $anno_1 ); $i++){
        echo "<li>";
        echo "<span class='nombre_pdf'>".
            "<a target='blank' href='".site_url()."/wp-content/uploads/documentos/boletines/".$anno_1[$i]['nombre']."'>Descarga PDF</a>".
            "</span>".
            "<span class='anno_pdf'>".
            $anno_1[$i]['año'].
            "</span>".
            "<span class='mes_pdf'>".
            get_month($anno_1[$i]['mes']).
            "</span>";
        echo "<input type='hidden' id='fechames' name='mes' value='".get_month($anno_1[$i]['mes'])."'><input type='hidden' id='fechaaño' name='año' value='".$anno_1[$i]['año']."'></il>";
    }
    echo "</ul>";

}
?>


<?php

wp_footer();
get_footer();

?>